import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class EnterPoint
{
	public static void main(String[] args)
	{
		System.out.println("Podaj imie:");
		Scanner scanner = new Scanner(System.in);
		String name = scanner.nextLine();
		System.out.println("Podaj liczbę 1:");
		double number1 = scanner.nextDouble();
		System.out.println("Podaj liczbę 2:");
		double numver2 = scanner.nextDouble();
		System.out.println("Witaj: "+name+"\nnr1: "+number1+" nr2: "+numver2+" nr1 + nr2 = "+(number1+numver2));

		Date date = Calendar.getInstance().getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
		String strDate = dateFormat.format(date);

		System.out.println("Bierząca data i czas: "+strDate);
	}
}
