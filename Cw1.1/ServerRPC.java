/**
 * Kamil Socha, 238054
 */
import org.apache.xmlrpc.WebServer;

//10.182.238.74

public class ServerRPC
{
	private static int PORT = 10000+13;

	public static void main(String[] args)
	{
		try
		{
			System.out.println("Startuje serwer XML-RPC...");
			int port = PORT;
			WebServer server = new WebServer(port);

			server.addHandler("MojSerwer", new ServerRPC());
			server.start();
			System.out.println("Serwer wystartowal pomyslnie.");
			System.out.println("Nasluchuje na porcie: " + port);
			System.out.println("Aby zatrzymać serwer nacisnij         crl+c");
		}
		catch (Exception exception)
		{
			System.err.println("Serwer XML-RPC: " + exception);
		}
	}

	public Integer Echo(int x, int y)
	{
		return new Integer(x+y);
	}

	public int execAsy(int x)
	{
		System.out.println("... wywołano asy - odliczam");
		try
		{
			Thread.sleep(x);
		}
		catch(InterruptedException ex)
		{
			ex.printStackTrace();
			Thread.currentThread().interrupt();
		}
		System.out.println("... asy - koniec odliczania");
		return (1000000);
	}
}
