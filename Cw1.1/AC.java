/**
 * Kamil Socha, 238054
 */
import org.apache.xmlrpc.AsyncCallback;

import java.net.URL;

public class AC implements AsyncCallback
{

	@Override
	public void handleResult(Object o, URL url, String s)
	{
		System.out.println("Handle result: o{"+o+"}; url:{"+url+"}; s:{"+s+"};");
	}

	@Override
	public void handleError(Exception e, URL url, String s)
	{
		System.out.println("Handle error: e:{"+e+"}; url:{"+url+"}; s:{"+s+"};");
	}
}
