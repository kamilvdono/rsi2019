/**
 * Kamil Socha, 238054
 */

import org.apache.xmlrpc.WebServer;
import org.apache.xmlrpc.XmlRpcClient;

import java.util.Scanner;
import java.util.Vector;

//10.182.238.74

public class Main
{
	private static int PORT = 10000+13;

	public static void main(String[] args)
	{
		AC ac = new AC();

		System.out.println("Podaj adres ip:");
		Scanner scanner = new Scanner(System.in);
		String ip = scanner.nextLine();
		System.out.println("Podaj port:");
		String port = scanner.nextLine();
		try
		{
			XmlRpcClient srv = new XmlRpcClient("http://"+ip+":"+port);

			Vector<Integer> params2 = new Vector<Integer>();
			params2.addElement(new Integer(3000));
			srv.executeAsync("MojSerwer.execAsy", params2, ac);
			System.out.println("Wywolano asynchronicznie");

			Vector<Integer> params = new Vector<Integer>();
			params.addElement(new Integer(13));
			params.addElement(new Integer(21));
			Object result = srv.execute("MojSerwer.Echo", params);
			int wynik = ((Integer) result).intValue();
			System.out.println("Wynik: "+wynik);

			
		}
		catch (Exception exception)
		{
			System.err.println("Klient XML-RPC: " +exception);
		}
	}
}
