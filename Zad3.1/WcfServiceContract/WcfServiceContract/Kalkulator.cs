﻿//Kamil Socha, 238054

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfServiceContract
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
	public class Kalkulator : IKalkulator
	{
		public int Sum(int a, int b)
		{
			return a + b;
		}

		public int Sub(int a, int b)
		{
			return a - b;
		}

		public int Mul(int a, int b)
		{
			return a * b;
		}

		public int Div(int a, int b)
		{
			if (b == 0)
			{
				DivByZero fault = new DivByZero();

				fault.Result = false;
				fault.Message = "Attempted to divide by zero.";
				fault.Description = "Cannot divide by zero in divide.";

				throw new FaultException<DivByZero>(fault);
			}
			return a / b;
		}

		public int Mod(int a, int b)
		{
			if (b == 0)
			{
				DivByZero fault = new DivByZero();

				fault.Result = false;
				fault.Message = "Attempted to divide by zero.";
				fault.Description = "Cannot divide by zero in modulo.";

				throw new FaultException<DivByZero>(fault);
			}
			return a % b;
		}

		public double Pow(int a, int b)
		{
			return Math.Pow(a, b);
		}
	}
}
