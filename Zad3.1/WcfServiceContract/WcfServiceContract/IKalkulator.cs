﻿//Kamil Socha, 238054

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfServiceContract
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
	[ServiceContract]
	public interface IKalkulator
	{
		[OperationContract]
		int Sum(int a, int b);

		[OperationContract]
		int Sub(int a, int b);

		[OperationContract]
		int Mul(int a, int b);

		[OperationContract]
		[FaultContract(typeof(DivByZero))]
		int Div(int a, int b);

		[OperationContract]
		[FaultContract(typeof(DivByZero))]
		int Mod(int a, int b);

		[OperationContract]
		double Pow(int a, int b);
	}

	[DataContract]
	public class DivByZero
	{
		[DataMember]
		public bool Result { get; set; }
		[DataMember]
		public string Message { get; set; }
		[DataMember]
		public string Description { get; set; }
	}
}
