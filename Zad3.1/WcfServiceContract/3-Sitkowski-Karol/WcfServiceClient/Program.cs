﻿//Kamil Socha, 238054
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using WcfServiceClient.ServiceReference2;

namespace WcfServiceClient
{
	class Program
	{
		static void Main(string[] args)
		{
			//Krok 1: Utworzenie instancji WCF proxy.
			//KalkulatorClient mojKlient = new KalkulatorClient();

			string[] endpointNames =
				{"WSHttpBinding_IKalkulator1", "NetTcpBinding_IKalkulator1"};

			KalkulatorClient[] clients = new KalkulatorClient[endpointNames.Length];

			for (int i = 0; i < endpointNames.Length; i++)
			{
				clients[i] = new KalkulatorClient(endpointNames[i]);
			}

			foreach (var kalkulatorClient in clients)
			{
				Console.WriteLine($"Wywłonia dla endpointa {kalkulatorClient.Endpoint.Name}");

				int value1 = 15;
				int value2 = 5;


				int result = kalkulatorClient.Sum(value1, value2);
				Console.WriteLine($"Wywołano dodaj({value1}, {value2}) - wynik : {result}");

				// Operacja Odejmij:       result
				result = kalkulatorClient.Sub(value1, value2);
				Console.WriteLine($"Wywołano odejmij({value1}, {value2}) - wynik : {result}");

				// Operacja Pomnoz:       result
				result = kalkulatorClient.Mul(value1, value2);
				Console.WriteLine($"Wywołano pomnoz({value1}, {value2}) - wynik : {result}");

				// Operacja Podziel:       result
				try
				{
					result = kalkulatorClient.Div(value1, value2);
					Console.WriteLine($"Wywołano podziel({value1}, {value2}) - wynik : {result}");
				}
				catch (FaultException<ServiceReference2.DivByZero> e)
				{
					Console.WriteLine("Message: {0}, Description: {1}", e.Detail.Message, e.Detail.Description);
				}
				catch (Exception e)
				{
					Console.WriteLine(e.Message);
				}

				try
				{
					result = kalkulatorClient.Mod(value1, value2);
					Console.WriteLine($"Wywołano reszte z dzielenia z {value1}, {value2} - wynik {result}");
				}
				catch (FaultException<ServiceReference2.DivByZero> e)
				{
					Console.WriteLine("Message: {0}, Description: {1}", e.Detail.Message, e.Detail.Description);
				}
				catch (Exception e)
				{
					Console.WriteLine(e.Message);
				}


				double result2 = kalkulatorClient.Pow(value1, value2);
				Console.WriteLine($"Wywołano potege z {value1}, {value2} - wynik {result2}");

				// Krok 3: Zamknięcie klienta zamyka polaczenie i czysci           zasoby.
				//kalkulatorClient.Close();
			}

			Console.WriteLine("----");
			Console.WriteLine("----");
			Console.WriteLine("----");

			foreach (var kalkulatorClient in clients)
			{
				Console.WriteLine($"Wywłonia dla endpointa {kalkulatorClient.Endpoint.Name}");

				int value1 = 15;
				int value2 = 0;


				int result = kalkulatorClient.Sum(value1, value2);
				Console.WriteLine($"Wywołano dodaj({value1}, {value2}) - wynik : {result}");

				// Operacja Odejmij:       result
				result = kalkulatorClient.Sub(value1, value2);
				Console.WriteLine($"Wywołano odejmij({value1}, {value2}) - wynik : {result}");

				// Operacja Pomnoz:       result
				result = kalkulatorClient.Mul(value1, value2);
				Console.WriteLine($"Wywołano pomnoz({value1}, {value2}) - wynik : {result}");

				// Operacja Podziel:       result
				try
				{
					result = kalkulatorClient.Div(value1, value2);
					Console.WriteLine($"Wywołano podziel({value1}, {value2}) - wynik : {result}");
				}
				catch (FaultException<ServiceReference2.DivByZero> e)
				{
					Console.WriteLine("Message: {0}, Description: {1}", e.Detail.Message, e.Detail.Description);
				}
				catch (Exception e)
				{
					Console.WriteLine(e.Message);
				}

				try
				{
					result = kalkulatorClient.Mod(value1, value2);
					Console.WriteLine($"Wywołano reszte z dzielenia z {value1}, {value2} - wynik {result}");
				}
				catch (FaultException<ServiceReference2.DivByZero> e)
				{
					Console.WriteLine("Message: {0}, Description: {1}", e.Detail.Message, e.Detail.Description);
				}
				catch (Exception e)
				{
					Console.WriteLine(e.Message);
				}


				double result2 = kalkulatorClient.Pow(value1, value2);
				Console.WriteLine($"Wywołano potege z {value1}, {value2} - wynik {result2}");

				// Krok 3: Zamknięcie klienta zamyka polaczenie i czysci           zasoby.
				kalkulatorClient.Close();
			}
		}
	}
}
