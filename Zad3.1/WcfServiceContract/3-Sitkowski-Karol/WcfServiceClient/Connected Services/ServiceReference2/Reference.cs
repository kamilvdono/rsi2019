﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Ten kod został wygenerowany przez narzędzie.
//     Wersja wykonawcza:4.0.30319.42000
//
//     Zmiany w tym pliku mogą spowodować nieprawidłowe zachowanie i zostaną utracone, jeśli
//     kod zostanie ponownie wygenerowany.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WcfServiceClient.ServiceReference2 {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="DivByZero", Namespace="http://schemas.datacontract.org/2004/07/WcfServiceContract")]
    [System.SerializableAttribute()]
    public partial class DivByZero : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string DescriptionField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MessageField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool ResultField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Description {
            get {
                return this.DescriptionField;
            }
            set {
                if ((object.ReferenceEquals(this.DescriptionField, value) != true)) {
                    this.DescriptionField = value;
                    this.RaisePropertyChanged("Description");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Message {
            get {
                return this.MessageField;
            }
            set {
                if ((object.ReferenceEquals(this.MessageField, value) != true)) {
                    this.MessageField = value;
                    this.RaisePropertyChanged("Message");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool Result {
            get {
                return this.ResultField;
            }
            set {
                if ((this.ResultField.Equals(value) != true)) {
                    this.ResultField = value;
                    this.RaisePropertyChanged("Result");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceReference2.IKalkulator")]
    public interface IKalkulator {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IKalkulator/Sum", ReplyAction="http://tempuri.org/IKalkulator/SumResponse")]
        int Sum(int a, int b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IKalkulator/Sum", ReplyAction="http://tempuri.org/IKalkulator/SumResponse")]
        System.Threading.Tasks.Task<int> SumAsync(int a, int b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IKalkulator/Sub", ReplyAction="http://tempuri.org/IKalkulator/SubResponse")]
        int Sub(int a, int b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IKalkulator/Sub", ReplyAction="http://tempuri.org/IKalkulator/SubResponse")]
        System.Threading.Tasks.Task<int> SubAsync(int a, int b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IKalkulator/Mul", ReplyAction="http://tempuri.org/IKalkulator/MulResponse")]
        int Mul(int a, int b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IKalkulator/Mul", ReplyAction="http://tempuri.org/IKalkulator/MulResponse")]
        System.Threading.Tasks.Task<int> MulAsync(int a, int b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IKalkulator/Div", ReplyAction="http://tempuri.org/IKalkulator/DivResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(WcfServiceClient.ServiceReference2.DivByZero), Action="http://tempuri.org/IKalkulator/DivDivByZeroFault", Name="DivByZero", Namespace="http://schemas.datacontract.org/2004/07/WcfServiceContract")]
        int Div(int a, int b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IKalkulator/Div", ReplyAction="http://tempuri.org/IKalkulator/DivResponse")]
        System.Threading.Tasks.Task<int> DivAsync(int a, int b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IKalkulator/Mod", ReplyAction="http://tempuri.org/IKalkulator/ModResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(WcfServiceClient.ServiceReference2.DivByZero), Action="http://tempuri.org/IKalkulator/ModDivByZeroFault", Name="DivByZero", Namespace="http://schemas.datacontract.org/2004/07/WcfServiceContract")]
        int Mod(int a, int b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IKalkulator/Mod", ReplyAction="http://tempuri.org/IKalkulator/ModResponse")]
        System.Threading.Tasks.Task<int> ModAsync(int a, int b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IKalkulator/Pow", ReplyAction="http://tempuri.org/IKalkulator/PowResponse")]
        double Pow(int a, int b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IKalkulator/Pow", ReplyAction="http://tempuri.org/IKalkulator/PowResponse")]
        System.Threading.Tasks.Task<double> PowAsync(int a, int b);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IKalkulatorChannel : WcfServiceClient.ServiceReference2.IKalkulator, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class KalkulatorClient : System.ServiceModel.ClientBase<WcfServiceClient.ServiceReference2.IKalkulator>, WcfServiceClient.ServiceReference2.IKalkulator {
        
        public KalkulatorClient() {
        }
        
        public KalkulatorClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public KalkulatorClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public KalkulatorClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public KalkulatorClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public int Sum(int a, int b) {
            return base.Channel.Sum(a, b);
        }
        
        public System.Threading.Tasks.Task<int> SumAsync(int a, int b) {
            return base.Channel.SumAsync(a, b);
        }
        
        public int Sub(int a, int b) {
            return base.Channel.Sub(a, b);
        }
        
        public System.Threading.Tasks.Task<int> SubAsync(int a, int b) {
            return base.Channel.SubAsync(a, b);
        }
        
        public int Mul(int a, int b) {
            return base.Channel.Mul(a, b);
        }
        
        public System.Threading.Tasks.Task<int> MulAsync(int a, int b) {
            return base.Channel.MulAsync(a, b);
        }
        
        public int Div(int a, int b) {
            return base.Channel.Div(a, b);
        }
        
        public System.Threading.Tasks.Task<int> DivAsync(int a, int b) {
            return base.Channel.DivAsync(a, b);
        }
        
        public int Mod(int a, int b) {
            return base.Channel.Mod(a, b);
        }
        
        public System.Threading.Tasks.Task<int> ModAsync(int a, int b) {
            return base.Channel.ModAsync(a, b);
        }
        
        public double Pow(int a, int b) {
            return base.Channel.Pow(a, b);
        }
        
        public System.Threading.Tasks.Task<double> PowAsync(int a, int b) {
            return base.Channel.PowAsync(a, b);
        }
    }
}
