
/**
 * Kamil Socha, 238054
 */
import org.apache.xmlrpc.WebServer;
import java.util.Vector;

//10.182.238.74

public class ServerRPC {
	private static int PORT = 10000 + 14;

	public static void main(String[] args) {
		try {
			System.out.println("Startuje serwer XML-RPC...");
			int port = PORT;
			WebServer server = new WebServer(port);

			server.addHandler("MathServer", new ServerRPC());
			server.start();
			System.out.println("Serwer wystartowal pomyslnie.");
			System.out.println("Nasluchuje na porcie: " + port);
			System.out.println("Aby zatrzymać serwer nacisnij         crl+c");
		} catch (Exception exception) {
			System.err.println("Serwer XML-RPC: " + exception);
		}
	}

	public String Show() {
		return 
		"int CharCount(String string, String character, int sleepTime) - czeka sleepTime milisekund po czym zwraca ilosc wystapien znaku character w lancuchu string\n"
		+ "Vector<Integer> PrimeCount(int from, int n) - zwraca ilosc liczb pierwszych w przedziale <from;n>\n"
		+ "int Add(int x, int y) - zwraca sume x i y\n" + "int Mul(int x, int y) - zwraca iloczyn x i y\n"
		+ "double Lerp(double a, double b, double t) - zwraca wartosc liniowej extrapolacji pomiedzy wartoscia a i b przez interpolant t\n"
		+ "String MulString(int count, String string, int sleepTime) - zwraca wyrazenie string zduplikowane count razy, zanim zwroci wynik czeka sleepTime milisekund\n"
		+ "String Show() - zwraca publiczny interfejs serwera\n";
	}

	public int Add(int x, int y) {
		return new Integer(x + y);
	}

	public int Mul(int x, int y) {
		return x * y;
	}

	public double Lerp(double a, double b, double t) {
		if (t < 0)
			t = 0;
		else if (t > 1)
			t = 1;
		return a * (1 - t) + b * t;
	}

	public String MulString(int count, String string, int sleepTime) {
		String r = "";
		for (int i = 0; i < count; i++) {
			r += string;
		}

		try {
			Thread.sleep(sleepTime);
		} catch (InterruptedException ex) {
			ex.printStackTrace();
			Thread.currentThread().interrupt();
		}

		return r;
	}

	public int CharCount(String string, String character, int sleepTime) {
		try {
			Thread.sleep(sleepTime);
		} catch (InterruptedException ex) {
			ex.printStackTrace();
			Thread.currentThread().interrupt();
		}

		int count = string.length() - string.replace(character, "").length();

		return count;
	}

	public Vector<Integer> PrimeCount(int from, int n) {
		n++;
		boolean prime[] = new boolean[n + 1];
		for (int i = 0; i < n; i++)
			prime[i] = true;

		for (int p = 2; p * p <= n; p++) {
			if (prime[p] == true) {
				for (int i = p * p; i <= n; i += p)
					prime[i] = false;
			}
		}

		Vector<Integer> params = new Vector<Integer>();
		for (int i = from; i <= n; i++) {
			if (prime[i] == true) {
				params.add(i);
			}
		}
		return params;
	}
}
