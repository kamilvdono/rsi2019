﻿//Kamil Socha, 238054
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using WcfServiceConcract;

namespace Host
{
	class Program
	{
		static void Main(string[] args)
		{
			Uri baseAddress = new Uri("http://localhost:10000/CD");

			ServiceHost mojHost = new ServiceHost(typeof(KalkulatorLz), baseAddress);
			try
			{
				// Krok 3 Dodaj endpoint.
				WSHttpBinding mojBanding = new WSHttpBinding();
				mojHost.AddServiceEndpoint(typeof(IKalkulatorLz), mojBanding, "endpoint1");
				// Krok 4 Ustaw wymiane metadanych.
				ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
				smb.HttpGetEnabled = true;
				mojHost.Description.Behaviors.Add(smb);
				// Krok 5 Uruchom serwis.
				mojHost.Open();
				Console.WriteLine("Serwis jest uruchomiony.");
				Console.WriteLine("Nacisnij <ENTER> aby zakonczyc.");
				Console.WriteLine();
				Console.ReadLine();
				mojHost.Close();
			}
			catch (CommunicationException ce)
			{
				Console.WriteLine("Wystapil wyjatek: {0}", ce.Message);
				mojHost.Abort();
			}
		}
	}
}
