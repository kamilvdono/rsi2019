﻿//Kamil Socha, 238054
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfServiceConcract
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
	[ServiceContract]
	public interface IKalkulatorLz
	{
		[OperationContract]
		LiczbaZ DodajLZ(LiczbaZ n1, LiczbaZ n2);
	}

	[DataContract]
	public class LiczbaZ
	{
		[DataMember] public double czescR;
		[DataMember] public double czescU;

		public LiczbaZ(double czesc_rz, double czesc_ur)
		{
			this.czescR = czesc_rz; this.czescU = czesc_ur;
		}
	}
}
