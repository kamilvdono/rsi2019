﻿//Kamil Socha, 238054
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfServiceConcract
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
	public class KalkulatorLz : IKalkulatorLz
	{
		public LiczbaZ DodajLZ(LiczbaZ n1, LiczbaZ n2)
		{
			Console.WriteLine("...wywolano DodajLZ(...)");
			return new LiczbaZ(n1.czescR + n2.czescR, n1.czescU + n2.czescU);
		}
	}
}
