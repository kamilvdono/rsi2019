﻿//Kamil Socha, 238054
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.ServiceReference1;

namespace Client
{
	class Program
	{
		static void Main(string[] args)
		{
			KalkulatorLzClient client1 = new KalkulatorLzClient();
			LiczbaZ lz1 = new LiczbaZ();
			lz1.czescR = 1.2;
			lz1.czescU = 3.4;
			LiczbaZ lz2 = new LiczbaZ(); lz2.czescR = 1.2;
			lz2.czescU = 3.4; Console.WriteLine("\nKLIENT1");
			Console.WriteLine("...wywoluje DodajLZ(...)");
			LiczbaZ result1 = client1.DodajLZ(lz1, lz2);
			Console.WriteLine(" DodajLZ(...) = ({0},{1})", result1.czescR, result1.czescU);
			client1.Close();
			Console.WriteLine("KONIEC KLIENT1");
			Console.ReadLine();
		}
	}
}
