//Kamil Socha 238054

import java.util.ArrayList;
//
public class Main {
    public static void main(String[] args) {
        System.out.println("Client started");
        Worker worker1, worker2, worker3;
        ResultType result, result1, result2;
        String address = "//10.182.238.74:1099/server1";
        String address2 = "//10.182.238.74:1099/server2";
        String address3 = "//10.182.238.74:1099/server3";
        ArrayList<Person> list1;
        ArrayList<Person> list2;

        try{

            worker1 = (Worker) java.rmi.Naming.lookup(address);
            worker2 = (Worker) java.rmi.Naming.lookup(address2);
            worker3 = (Worker) java.rmi.Naming.lookup(address3);
        }catch (Exception e){
            e.printStackTrace();
            return;
        }
        try {
            list1 = new ArrayList<>();
            list1.add(new Person("Jan","Ananas"));
            list1.add(new Person("Karol","Cebula"));
            list1.add(new Person("Kamil","Cebula"));
            list1.add(new Person("Kamil","Ogorek"));
            list1.add(new Person("Wojciech","Ogorek"));
            list1.add(new Person("Karolina", "Karolewicz"));
            list1.add(new Person("Jan", "Janowski"));


            list2 = new ArrayList<>();
            list2.add(new Person("Katarzyna","Ryba"));
            list2.add(new Person("Monika","Pies"));
            list2.add(new Person("Alina","Wieloryb"));
            list2.add(new Person("Alicja","Solo"));
            list2.add(new Person("Adam", "Adamski"));

            Task task1 = new TaskImpl(list1,"J");
            Task task2 = new TaskImpl(list2,"A");

            result1 = worker1.compute(task1);
            result2 = worker2.compute(task2);
            Task task3 = new TaskImpl(result1.result,result2.result);

            result = worker3.concat(task3);
            for (Person person : result.result) {
                System.out.print(person.name + ", " + person.lastName);
                System.out.println();
            }

        }catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
