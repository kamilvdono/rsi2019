//Kamil Socha 238054

import java.lang.reflect.Array;
import java.util.ArrayList;

public class TaskImpl implements Task {
    public TaskImpl(ArrayList<Person> list,String name) {
        this.list = list;
        list = new ArrayList<>();
        this.name = name;
    }
    public TaskImpl(ArrayList<Person> list1, ArrayList<Person> list2)
    {
        this.list1 = list1;
        list1 = new ArrayList<>();
        this.list2 = list2;
        list2 = new ArrayList<>();
    }

    private  static final long serialVersionUID = 102L;
    ArrayList<Person> list;
    ArrayList<Person> list1;
    ArrayList<Person> list2;
    String name;

    @Override
    public ResultType concat() {
        ResultType resultType= new ResultType();
        resultType.result.addAll(list1);
        resultType.result.addAll(list2);

        return resultType;
    }

    @Override
    public ResultType compute() {
        ResultType resultType = new ResultType();

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).name.charAt(0) == (name.charAt(0)))
            {
                resultType.result.add(list.get(i));
            }
        }
        return resultType;
    }
}

