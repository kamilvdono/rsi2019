//Kamil Socha 238054

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Worker extends Remote {
    public ResultType compute(Task task) throws RemoteException;
    public ResultType concat(Task task) throws RemoteException;

}
