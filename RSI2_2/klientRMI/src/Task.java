//Kamil Socha 238054

import java.io.Serializable;

public interface Task extends Serializable {
    public ResultType compute();
    public ResultType concat();
}
