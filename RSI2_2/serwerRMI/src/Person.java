//Kamil Socha 238054

import java.io.Serializable;

public class Person implements Serializable {
    private  static final long serialVersionUID = 103L;
    public String name;
    public String lastName;
    public Person(String name, String lastName)
    {
        this.name = name;
        this.lastName = lastName;
    }
}
