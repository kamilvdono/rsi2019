//Kamil Socha 238054

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class WorkerImpl extends UnicastRemoteObject implements Worker{

    public WorkerImpl() throws RemoteException
    {
        super();
    }
    @Override
    public ResultType compute(Task task) throws RemoteException {
        return task.compute();
    }

    @Override
    public ResultType concat(Task task) throws RemoteException {
        return task.concat();
    }
}
