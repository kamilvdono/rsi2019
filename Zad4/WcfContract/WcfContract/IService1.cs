//Kamil Socha, 238054
using System.ServiceModel;

namespace WcfContract
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
	[ServiceContract]
	public interface IService1
	{
		[OperationContract]
		void Function1( string a );

		[OperationContract( IsOneWay = true )]
		void Funtion2( string a );
	}
}
