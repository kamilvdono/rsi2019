//Kamil Socha, 238054
using System;
using System.ServiceModel;
using System.Threading;

namespace WcfContract
{
	[ServiceBehavior( ConcurrencyMode = ConcurrencyMode.Multiple )]
	public class Service1 : IService1
	{
		public void Function1( string a )
		{
			Console.WriteLine( "...{0}: funkcja1 - start", a );
			Thread.Sleep( 3000 );
			Console.WriteLine( "...{0}: funkcja1 - stop", a );
			return;
		}

		public void Funtion2( string a )
		{
			Console.WriteLine( "...{0}: funkcja2 - start", a );
			Thread.Sleep( 3000 );
			Console.WriteLine( "...{0}: funkcja2 - stop", a );
			return;
		}
	}
}
