//Kamil Socha, 238054

using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using WcfCallbackContract;
using WcfContract;

namespace WcfHost
{
	internal class Program
	{
		private static void Main( string[] args )
		{
			Uri baseAddress1 = new Uri("http://localhost:8090/serv/serv1");
			ServiceHost mojHost1 = new ServiceHost(typeof(Service1), baseAddress1);

			Uri calculatorAddress = new Uri("http://localhost:8090/serv/calc");
			ServiceHost calculatorHost = new ServiceHost(typeof(Calculator), calculatorAddress);

			try
			{
				ServiceEndpoint endpoint1 = mojHost1.AddServiceEndpoint(typeof(IService1), new WSHttpBinding(), "");
				ServiceEndpoint calculatorEndpoint = calculatorHost.AddServiceEndpoint(typeof(ICalculator), new WSDualHttpBinding(), "");

				ServiceMetadataBehavior smb = new ServiceMetadataBehavior
				{
					HttpGetEnabled = true
				};
				mojHost1.Description.Behaviors.Add( smb );
				ServiceMetadataBehavior smb1 = new ServiceMetadataBehavior
				{
					HttpGetEnabled = true,
				};
				calculatorHost.Description.Behaviors.Add( smb1 );

				mojHost1.Open();
				Console.WriteLine( "--->MojSerwis jest uruchomiony." );
				calculatorHost.Open();
				Console.WriteLine( "--->Calculator jest uruchomiony." );

				Console.WriteLine( "--->Nacisnij <ENTER> aby zakonczyc.\n" );
				Console.ReadLine();     //czekam na zamkniecie  

				mojHost1.Close();
				calculatorHost.Close();

				Console.WriteLine( "---> Serwis zakonczyl dzialanie." );
			}
			catch ( CommunicationException ce )
			{
				Console.WriteLine( "Wystapil wyjatek: {0}", ce.Message );
				mojHost1.Abort();
			}
		}
	}
}
