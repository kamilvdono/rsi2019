//Kamil Socha, 238054

using System.ServiceModel;

namespace WcfCallbackContract
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
	[ServiceContract( SessionMode = SessionMode.Required, CallbackContract = typeof( ICallbackHandler ) )]
	public interface ICalculator
	{
		[OperationContract( IsOneWay = true )]
		void Silnia( double n );

		[OperationContract( IsOneWay = true )]
		void ObliczCos( int sek );
	}

	public interface ICallbackHandler
	{
		[OperationContract( IsOneWay = true )]
		void ZwrotSilnia( double result );

		[OperationContract( IsOneWay = true )]
		void ZwrotObliczCos( string result );
	}
}
