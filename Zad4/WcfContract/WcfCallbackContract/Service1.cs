//Kamil Socha, 238054

using System;
using System.ServiceModel;
using System.Threading;

namespace WcfCallbackContract
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
	[ServiceBehavior( InstanceContextMode = InstanceContextMode.PerSession )]
	public class Calculator : ICalculator
	{
		private double result;
		private ICallbackHandler callback = null;

		public Calculator()
		{
			callback = OperationContext.Current.GetCallbackChannel<ICallbackHandler>();
		}

		public void ObliczCos( int sek )
		{
			Console.WriteLine( "...wywolano Oblicz({0})", sek );
			if ( sek < 10 )
			{
				Thread.Sleep( sek * 1000 );
			}
			else
			{
				Thread.Sleep( 1000 );
			}

			callback.ZwrotObliczCos( "Obliczenia trwaly " + ( sek + 1 ) + " sekund(y)" );
		}
		public void Silnia( double n )
		{
			Console.WriteLine( "...wywolano Silnia({0})", n );
			Thread.Sleep( 1000 ); result = 1;
			for ( int i = 1; i <= n; i++ )
			{
				result *= i;
			}
			callback.ZwrotSilnia( result );
		}
	}
}
