//Kamil Socha, 238054

using System;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace StreamHost
{
	internal class Program
	{
		private static void Main( string[] args )
		{
			Uri streamAddress = new Uri("http://localhost:8090/");
			ServiceHost streamHost = new ServiceHost(typeof(StreamContract.StreamService), streamAddress);

			try
			{
				BasicHttpBinding b = new BasicHttpBinding
				{
					TransferMode = TransferMode.Streamed,
					MaxReceivedMessageSize = 1000000000,
					MaxBufferSize = 8192
				};
				ServiceEndpoint endpoint = streamHost.AddServiceEndpoint(typeof(StreamContract.IStreamService), b, streamAddress);

				ServiceMetadataBehavior smb = new ServiceMetadataBehavior
				{
					HttpGetEnabled = true
				};
				streamHost.Description.Behaviors.Add( smb );

				streamHost.Open();
				Console.WriteLine( "--->Stream jest uruchomiony." );
				Console.ReadLine();
				Console.WriteLine( "Zakończono" );
			}
			catch ( CommunicationException ce )
			{
				Console.WriteLine( "Wystapil wyjatek: {0}", ce.Message );
				streamHost.Abort();
			}
		}
	}
}
