//Kamil Socha, 238054

using StreamClient.ServiceReference1;
using System;
using System.IO;

namespace StreamClient
{
	internal class Program
	{
		private static void Main( string[] args )
		{
			StreamServiceClient streamServiceClient = new ServiceReference1.StreamServiceClient();
			string filePath = Path.Combine( System.Environment.CurrentDirectory, "klient.jpg");

			Console.WriteLine( "Wywoluje GetStream()" );
			System.IO.Stream stream = streamServiceClient.GetStream("btoom.jpg");
			SaveFile( stream, filePath );

			Console.WriteLine( "Wywoluje GetMSStream()" );
			string name = "btoom.jpg";
			long fileSize = streamServiceClient.GetMStream(ref name, out stream);
			filePath = Path.Combine( System.Environment.CurrentDirectory, name );
			SaveFile( stream, filePath );

			streamServiceClient.Close();
			Console.WriteLine();
			Console.WriteLine( "Nacisnij <ENTER> aby zakonczyc." );
			Console.ReadLine();
		}

		private static void SaveFile( Stream stream, string filePath )
		{
			const int bufferLength = 8192;    //długość bufora 8KB 
			int bytecount = 0;      //licznik  
			int counter = 0;      //licznik pomocniczy 

			byte[] buffer = new byte[bufferLength];
			Console.WriteLine( "--> Zapisuje plik {0}", filePath );
			FileStream outstream = File.Open(filePath, FileMode.Create, FileAccess.Write);   //zapisywanie danych porcjami   
			while ( ( counter = stream.Read( buffer, 0, bufferLength ) ) > 0 )
			{
				outstream.Write( buffer, 0, counter );
				Console.Write( ".{0}", counter );
				bytecount += counter;
			}
			Console.WriteLine();
			Console.WriteLine( "Zapisano {0} bajtow", bytecount );

			outstream.Close();
			stream.Close();
			Console.WriteLine();
			Console.WriteLine( "--> Plik {0} zapisany", filePath );
		}
	}
}
