﻿//Kamil Socha, 238054

using System;
using System.IO;
using System.Windows.Forms;

internal class FClinet
{
	[STAThread]
	private static void Main( string[] args )
	{
		FileServiceClientAp.FileService.FileServiceClient client = new FileServiceClientAp.FileService.FileServiceClient();
		var files = client.GetFilesNames();
		foreach ( var file in files )
		{
			Console.WriteLine( $"File at {file}" );
		}

		OpenFileDialog theDialog = new OpenFileDialog();
		theDialog.Title = "Open Text File";
		theDialog.Filter = "TXT files|*.png";
		if ( theDialog.ShowDialog() == DialogResult.OK )
		{
			try
			{
				var stream = theDialog.OpenFile();
				client.SendFile( Path.GetFileName( theDialog.FileName ), stream.Length, stream );
			}
			catch ( Exception ex )
			{
				MessageBox.Show( "Error: Could not read file from disk. Original error: " + ex.Message );
			}
		}
		Console.WriteLine( "Sended data" );
		Console.ReadLine();
	}
}