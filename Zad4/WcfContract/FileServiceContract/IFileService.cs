﻿//Kamil Socha, 238054
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;

namespace FileServiceContract
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
	[ServiceContract]
	public interface IFileService
	{
		[OperationContract]
		ResponseFileMessage GetFileStream( RequestFileMessage request );

		[OperationContract]
		void SendFile( ResponseFileMessage fileMetaData );

		[OperationContract]
		List<string> GetFilesNames();
	}

	[MessageContract]
	public class RequestFileMessage
	{
		[MessageBodyMember]
		public string Name;
	}

	[MessageContract]
	public class ResponseFileMessage
	{
		[MessageHeader]
		public string Name;
		[MessageHeader]
		public long Size;
		[MessageBodyMember]
		public Stream Stream;
	}
}
