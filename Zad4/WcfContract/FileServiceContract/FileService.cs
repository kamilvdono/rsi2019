﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FileServiceContract
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
	public class FileService : IFileService
	{
		public ResponseFileMessage GetFileStream( RequestFileMessage request )
		{
			FileStream myFile;
			Console.WriteLine( "GetFileStream" );
			string filePath = Path.Combine(System.Environment.CurrentDirectory, "Data", request.Name);
			try
			{
				myFile = File.OpenRead( filePath );
			}
			catch ( IOException ex )
			{
				Console.WriteLine( string.Format( "File exception {0} :", filePath ) );
				Console.WriteLine( ex.ToString() );
				throw ex;
			}
			return new ResponseFileMessage()
			{
				Name = $"{request.Name.Split( '.' )[0]}_{DateTime.Now.Ticks}.{request.Name.Split( '.' )[1]}",
				Size = myFile.Length,
				Stream = myFile,
			};
		}

		public List<string> GetFilesNames()
		{
			Console.WriteLine( "GetFilesNames" );
			string dataPath = Path.Combine(System.Environment.CurrentDirectory, "Data");
			return GetAllFilesRecursive( dataPath ).Select( p => Path.GetFileName( p ) ).ToList();
		}

		public static List<string> GetAllFilesRecursive( string directory )
		{
			List<string> files = new List<string>( Directory.GetFiles( directory ) );
			foreach ( string dir in Directory.GetDirectories( directory ) )
			{
				files.AddRange( GetAllFilesRecursive( dir ) );
			}
			return files;
		}

		public void SendFile( ResponseFileMessage fileMetaData )
		{
			Console.WriteLine( "Start downloading file" );
			FileStream targetStream = null;
			Stream sourceStream =  fileMetaData.Stream;

			string filePath = Path.Combine(System.Environment.CurrentDirectory, "Data", fileMetaData.Name);

			using ( targetStream = new FileStream( filePath, FileMode.Create, FileAccess.Write, FileShare.None ) )
			{
				const int bufferLen = 65000;
				byte[] buffer = new byte[bufferLen];
				int count = 0;
				while ( ( count = sourceStream.Read( buffer, 0, bufferLen ) ) > 0 )
				{
					targetStream.Write( buffer, 0, count );
				}
				targetStream.Close();
				sourceStream.Close();
			}
			Console.WriteLine( "Stop downloading file" );
		}
	}
}
