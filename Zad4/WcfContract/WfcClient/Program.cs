//Kamil Socha, 238054

using System;
using System.ServiceModel;
using System.Threading;

namespace WfcClient
{
	internal class Program
	{
		private static void Main( string[] args )
		{
			ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();
			Console.WriteLine( "...wywoluje funkcja 1:" );
			client.Function1( "Klient1" );
			Thread.Sleep( 10 );
			Console.WriteLine( "...kontynuacja po funkcji 1" );
			Console.WriteLine( "...wywoluje funkcja 2:" );
			client.Funtion2( "Klient1" );
			Thread.Sleep( 10 );
			Console.WriteLine( "...kontynuacja po funkcji 2" );
			Console.WriteLine( "...wywoluje funkcja 1:" );
			client.Function1( "Klient1" );
			Thread.Sleep( 10 );
			Console.WriteLine( "...kontynuacja po funkcji 1" );
			client.Close();
			Console.WriteLine( "KONIEC KLIENT1\n" );

			Console.WriteLine( "---" );
			Console.WriteLine( "---" );

			Console.WriteLine( "\nKLIENT2:" );
			CallbackHandler mojCallbackHandler = new CallbackHandler();
			InstanceContext instanceContext = new InstanceContext(mojCallbackHandler);
			ServiceReference2.CalculatorClient client2 = new ServiceReference2.CalculatorClient(instanceContext);
			double value1 = 10;
			Console.WriteLine( "...wywoluje Silnia({0})...", value1 );
			client2.Silnia( value1 );
			value1 = 20;
			Console.WriteLine( "...wywoluje Silnia({0})...", value1 );
			client2.Silnia( value1 );
			int value2 = 2;
			Console.WriteLine( "...wywoluje obliczenia cosia..." );
			client2.ObliczCos( value2 );
			Console.WriteLine( "...czekam chwile na odbior wynikow" );
			Thread.Sleep( 5000 );
			client2.Close();
			Console.WriteLine( "KONIEC KLIENT2" );
		}
	}
}
