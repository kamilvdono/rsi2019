//Kamil Socha, 238054
using System;
using WfcClient.ServiceReference2;

namespace WfcClient
{
	public class CallbackHandler : ICalculatorCallback
	{
		public void ZwrotObliczCos( string result )
		{
			Console.WriteLine( " Silnia = {0}", result );
		}

		public void ZwrotSilnia( double result )
		{
			Console.WriteLine( " Obliczenia: {0}", result );
		}
	}
}
