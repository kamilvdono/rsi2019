//Kamil Socha, 238054

using System.IO;
using System.ServiceModel;

namespace StreamContract
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
	[ServiceContract]
	public interface IStreamService
	{
		[OperationContract]
		Stream GetStream( string name );

		[OperationContract]
		ResponseFileMessage GetMStream( RequestFileMessage request );
	}

	[MessageContract]
	public class RequestFileMessage
	{
		[MessageBodyMember]
		public string Name;
	}

	[MessageContract]
	public class ResponseFileMessage
	{
		[MessageHeader]
		public string Name;
		[MessageHeader]
		public long Size;
		[MessageBodyMember]
		public Stream Stream;
	}
}
