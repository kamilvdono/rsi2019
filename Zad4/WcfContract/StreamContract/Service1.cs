//Kamil Socha, 238054

using System;
using System.IO;

namespace StreamContract
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
	public class StreamService : IStreamService
	{
		public ResponseFileMessage GetMStream( RequestFileMessage request )
		{
			FileStream myFile;
			Console.WriteLine( "-->Wywolano GetMStream" );
			string filePath = Path.Combine(System.Environment.CurrentDirectory, request.Name);
			try
			{
				myFile = File.OpenRead( filePath );
			}
			catch ( IOException ex )
			{
				Console.WriteLine( string.Format( "Wyjatek otwarcia pliku {0} :", filePath ) );
				Console.WriteLine( ex.ToString() );
				throw ex;
			}
			return new ResponseFileMessage()
			{
				Name = $"{request.Name.Split( '.' )[0]}_{DateTime.Now.Ticks}.{request.Name.Split( '.' )[1]}",
				Size = myFile.Length,
				Stream = myFile,
			};
		}

		public Stream GetStream( string name )
		{
			FileStream myFile;
			Console.WriteLine( "-->Wywolano GetStream" );
			string filePath = Path.Combine(System.Environment.CurrentDirectory, name);
			try
			{
				myFile = File.OpenRead( filePath );
			}
			catch ( IOException ex )
			{
				Console.WriteLine( string.Format( "Wyjatek otwarcia pliku {0} :", filePath ) );
				Console.WriteLine( ex.ToString() );
				throw ex;
			}
			return myFile;
		}
	}
}
