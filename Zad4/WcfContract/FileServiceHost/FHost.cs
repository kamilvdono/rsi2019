﻿//Kamil Socha, 238054

using System;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace FileServiceHost
{
	internal class FHost
	{
		private static void Main( string[] args )
		{

			Uri serviceAddres = new Uri("http://localhost:8090/files/serv1");
			ServiceHost serviceHost = new ServiceHost(typeof(FileServiceContract.FileService), serviceAddres);

			try
			{
				ServiceEndpoint endpoint = serviceHost.AddServiceEndpoint(typeof(FileServiceContract.IFileService),
					new BasicHttpBinding(){ MaxReceivedMessageSize = 2147483647, MaxBufferSize = 2147483647, TransferMode = TransferMode.Streamed }, "");

				ServiceMetadataBehavior smb = new ServiceMetadataBehavior
				{
					HttpGetEnabled = true
				};
				serviceHost.Description.Behaviors.Add( smb );

				serviceHost.Open();
				Console.WriteLine( "Service running" );

				Console.WriteLine( "Enter to exit\n" );
				Console.ReadLine();

				serviceHost.Close();

				Console.WriteLine( "Service stop running" );
			}
			catch ( CommunicationException ce )
			{
				Console.WriteLine( "Exception: {0}", ce.Message );
				serviceHost.Abort();
			}
		}
	}
}
