import java.rmi.RemoteException;
import java.rmi.*;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.*;
import java.rmi.registry.LocateRegistry;

public class Server {
	public static void main(String[] args) {
		if (args.length == 0) {
			System.out.println("You have to enter RMI object address in the form: //host_address/service_name");
			return;
		}

		try
		{
			Registry reg = LocateRegistry.createRegistry(1099);
		}
		catch(RemoteException e1) 
		{
			e1.printStackTrace();
		}

		if (System.getSecurityManager() == null)
			System.setSecurityManager(new SecurityManager());

		try {
			CalcObjImpl implObiektu = new CalcObjImpl();
			CalcObjImpl2 i2 = new CalcObjImpl2();
			java.rmi.Naming.rebind(args[0], implObiektu);
			java.rmi.Naming.rebind(args[1], i2);
		} catch (Exception e) {
			System.out.println("SERVER CAN'T BE REGISTERED!");
			e.printStackTrace();
			return;
		}

		System.out.println("Press Crl+C to stop...");
	}
}