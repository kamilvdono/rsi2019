﻿using System;
using System.Windows.Forms;

namespace Client
{
    
    public partial class DescriptionInputForm : Form
    {
        string fileName;
        string filePath;
        Form1 mainForm;

        
        public DescriptionInputForm(string fName, string fPath, Form1 mainForm)
        {
            fileName = fName;
            filePath = fPath;
            this.mainForm = mainForm;
            
            InitializeComponent();
            lab_imageName.Text = fileName;
        }

        
        private void button1_Click(object sender, EventArgs e)
        {
            string description = tb_description.Text;
            mainForm.uploadFile(filePath, description);
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void DescriptionInputForm_Load(object sender, EventArgs e)
        {

        }
    }
}
