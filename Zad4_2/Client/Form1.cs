using Client.ServiceReference1;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Client
{

	public partial class Form1 : Form
	{
		private List<string> names = new List<string>();
		private Dictionary<string, string> data = new Dictionary<string, string>();
		private ToolTip toolTip = new ToolTip();
		private Service1Client client = new Service1Client();


		public Form1()
		{
			InitializeComponent();
			listBox1.DataSource = names;
			but_refresh_Click( null, null );
		}


		private void but_upload_Click( object sender, EventArgs e )
		{
			OpenFileDialog fileDialog = new OpenFileDialog
			{
				Filter = "JPG Image | *.jpg;*.jpeg",
				FilterIndex = 0
			};

			if ( fileDialog.ShowDialog() == DialogResult.OK )
			{
				string selectedFilePath = fileDialog.FileName;
				string fileName = Path.GetFileName(selectedFilePath);

				DescriptionInputForm descDialog = new DescriptionInputForm(fileName, selectedFilePath, this);
				descDialog.Show();
			}

		}


		public void uploadFile( string filePath, string description )
		{
			string fileName = Path.GetFileName(filePath);

			using ( FileStream myFile = File.OpenRead( filePath ) )
			{
				try
				{
					client.uploadFile( description, fileName, myFile );
				}
				catch ( Exception ex )
				{
					Console.WriteLine( string.Format( "Error uploading image {0}", filePath ) );
					Console.WriteLine( ex.ToString() );
					throw ex;
				}
			}

			but_refresh_Click( null, null );
		}


		private void but_exit_Click( object sender, EventArgs e )
		{
			client.Close();
			Application.Exit();
		}


		private void but_refresh_Click( object sender, EventArgs e )
		{
			names.Clear();

			data = client.getFiles();
			names.AddRange( data.Select( d => $"{d.Key} - ({d.Value})" ) );
			listBox1.DataSource = null;
			listBox1.DataSource = names;
		}


		private void but_download_Click( object sender, EventArgs e )
		{
			if ( listBox1.SelectedItem != null )
			{
				SaveFileDialog saveDialog = new SaveFileDialog();
				string selected = ((string)listBox1.SelectedItem).Split(' ')[0];
				saveDialog.FileName = selected;
				saveDialog.Filter = "JPG Image | *.jpg;*.jpeg";

				if ( saveDialog.ShowDialog() == DialogResult.OK )
				{
					client.downloadFile( selected, out string description, out Stream stream );
					string filePath = saveDialog.FileName;
					downloadFile( stream, filePath );
				}

			}
		}


		private void listbox1_MouseMove( object sender, MouseEventArgs e )
		{
			int index = listBox1.IndexFromPoint(e.Location);

			if ( index != -1 && index < listBox1.Items.Count )
			{
				data.TryGetValue( (string)listBox1.Items[index], out string desc );
				if ( toolTip.GetToolTip( listBox1 ) != desc )
				{
					toolTip.SetToolTip( listBox1, desc );
				}
			}
			else
			{
				toolTip.SetToolTip( listBox1, string.Empty );
			}
		}


		private void downloadFile( System.IO.Stream instream, string filePath )
		{
			const int bufferLength = 8192;

			int counter = 0;
			byte[] buffer = new byte[bufferLength];

			using ( instream )
			using ( FileStream outstream = File.Open( filePath, FileMode.Create, FileAccess.Write ) )
			{
				while ( ( counter = instream.Read( buffer, 0, bufferLength ) ) > 0 )
				{
					outstream.Write( buffer, 0, counter );
				}
			}
		}

		private void Form1_Load( object sender, EventArgs e )
		{

		}
	}
}
