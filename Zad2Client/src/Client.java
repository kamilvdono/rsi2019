public class Client {
	public static void main(String[] args) {
		double wynik;
		ResultType wynik2;
		CalcObject zObiekt;
		CalcObject2 cO2;

		if (args.length == 0) {
			System.out.println(
					"You have to enter RMI object address in the form: // host_address/service_name ");
			return;
		}

		String adres = args[0];
		String adres2 = args[1];

		// //use this if needed // if (System.getSecurityManager() == null) //
		// System.setSecurityManager(new SecurityManager());

		try {
			zObiekt = (CalcObject) java.rmi.Naming.lookup(adres);
			cO2 = (CalcObject2) java.rmi.Naming.lookup(adres2);
		} catch (Exception e) {
			System.out.println("Nie mozna pobrac referencji do " + adres);
			e.printStackTrace();
			return;
		}
		System.out.println("Referencja do " + adres + " jest pobrana.");

		try {
			wynik = zObiekt.calculate(1.1, 2.2);
			InputType it = new InputType();
			it.x1 = 3;
			it.x2 = 4;
			it.operation = "add";
			wynik2 = cO2.calculate(it);
		} catch (Exception e) {
			System.out.println("Blad zdalnego wywolania.");
			e.printStackTrace();
			return;
		}
		System.out.println("Wynik = " + wynik);
		System.out.println("Wynik2 = " + wynik2.result);
		return;
	}
}