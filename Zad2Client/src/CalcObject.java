import java.rmi.RemoteException;
 import java.rmi.server.UnicastRemoteObject; 
 import java.rmi.*;
public interface CalcObject extends Remote 
{
	public double calculate(double a, double b) throws RemoteException;
 }