import java.io.Serializable;
import java.math.BigInteger;

public class FibonacciParam implements Serializable, CalculateParam
{
    private long n;

    public FibonacciParam(long n)
    {
        this.n = n;
    }

    public long getN()
    {
        return n;
    }

    public BigIntegerResult calculate()
    {
        if(n < 0)
        {
            throw new IllegalArgumentException("The n parameter is negative!");
        }
        long startTime = System.nanoTime();

        BigInteger n1 = BigInteger.valueOf(0L);
        BigInteger n2 = BigInteger.valueOf(1L);
        BigInteger n3;

        for(long i = 1; i <= n; i++)
        {
            n3 = n1.add(n2);
            n1 = n2;
            n2 = n3;
        }
        float time = ((System.nanoTime() - startTime)/1000000000f);
        return new BigIntegerResult(n, n1, time, getClass().getName());
    }
}
