import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;

public class Main
{
    private static final int invalidWorkerPropertiesErrorCode = 666;
    private static final int serverCantStartErrorCode = 999;

    public static void main(String [] args) throws RemoteException
    {
        initSecurityManager();

        final Map<String, UnicastRemoteObject> workers = getWorkerProperties();

        try
        {
            LocateRegistry.createRegistry(1099);
            for (Map.Entry<String, UnicastRemoteObject> worker : workers.entrySet())
            {
                Naming.rebind(worker.getKey(), worker.getValue());
            }

            System.out.println("Server is registered now ( ͡° ͜ʖ ͡°)");
            System.out.println("Press ctrl+c to stop...");
        }
        catch (Exception e)
        {
            System.out.println("Server can't be registered!");
            e.printStackTrace();
            System.exit(serverCantStartErrorCode);
        }
    }

    private static void initSecurityManager()
    {
        if(System.getSecurityManager() == null)
        {
            System.setSecurityManager(new SecurityManager());
        }
    }

    private static Map<String, UnicastRemoteObject> getWorkerProperties()
    {
        try
        {
            return new HashMap<String, UnicastRemoteObject>(){{
                put("//localhost/server1", new CalculationWorker());
                put("//localhost/server2", new CalculationWorker());
            }};
        }
        catch (Exception e)
        {
            System.out.println("Invalid worker properties!");
            e.printStackTrace();
            System.exit(invalidWorkerPropertiesErrorCode);
        }
        return null;
    }
}
