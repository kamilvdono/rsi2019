import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class CalculationWorker extends UnicastRemoteObject implements ICalculationWorker
{
    private static final long serialVersionUID = 1022421L;

    public CalculationWorker() throws RemoteException
    {
        super();
    }

    @Override
    public BigIntegerResult calculate(CalculateParam param)
    {
        return param.calculate();
    }
}
