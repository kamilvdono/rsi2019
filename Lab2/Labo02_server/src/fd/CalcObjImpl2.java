package fd;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class CalcObjImpl2 extends UnicastRemoteObject implements CalcObject2
{
    private static final long serialVersionUID = 102L;

    public CalcObjImpl2() throws RemoteException
    {
        super();
    }

    public ResultType calculate(InputType inputParam) throws RemoteException
    {
        double x1 = inputParam.getX1();
        double x2 = inputParam.getX2();

        switch (inputParam.getOperation())
        {
            case "add":
                return new ResultType("Operation: " + inputParam.getOperation(), x1 + x2);
            case "sub":
                return new ResultType("Operation: " + inputParam.getOperation(), x1 - x2);
            default:
                return new ResultType("Wrong operation!");
        }
    }

    public Boolean dummy() throws RemoteException
    {
        return true;
    }
}
