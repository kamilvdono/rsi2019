package fd;

import java.io.Serializable;

public class ResultType implements Serializable
{
    private static final long serialVersionUID = 102;
    private String resultDescription;
    private double result;

    public ResultType() { }

    public ResultType(String resultDescription)
    {
        this.resultDescription = resultDescription;
    }

    public ResultType(String resultDescription, double result)
    {
        this.resultDescription = resultDescription;
        this.result = result;
    }

    public String getResultDescription()
    {
        return resultDescription;
    }

    public double getResult()
    {
        return result;
    }

    public void setResultDescription(String resultDescription)
    {
        this.resultDescription = resultDescription;
    }

    public void setResult(double result)
    {
        this.result = result;
    }

    @Override
    public String toString()
    {
        return "Description: " + resultDescription + "\nResult: " + result;
    }
}
