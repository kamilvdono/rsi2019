package fd;

import java.io.Serializable;

public class InputType implements Serializable
{
    private static final long serialVersionUID = 101L;
    private String operation;
    private double x1;
    private double x2;

    public InputType(double x1, double x2, String operation)
    {
        this.x1 = x1;
        this.x2 = x2;
        this.operation = operation;
    }

    public String getOperation()
    {
        return operation;
    }

    public double getX1()
    {
        return x1;
    }

    public double getX2()
    {
        return x2;
    }

    public void setOperation(String operation)
    {
        this.operation = operation;
    }

    public void setX1(double x1)
    {
        this.x1 = x1;
    }

    public void setX2(double x2)
    {
        this.x2 = x2;
    }
}
