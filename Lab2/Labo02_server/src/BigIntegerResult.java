import java.io.Serializable;
import java.math.BigInteger;
import java.text.DecimalFormat;

public class BigIntegerResult implements Serializable
{
    private long n;
    private BigInteger calculatedValue;
    private float seconds;
    private String description;

    public BigIntegerResult(long n, BigInteger calculatedValue, float seconds, String description)
    {
        this.n = n;
        this.calculatedValue = calculatedValue;
        this.seconds = seconds;
        this.description = description;
    }

    @Override
    public String toString()
    {
        return description + " Calculated value for n = " + n + " is: " + calculatedValue +
                ".\nCalculation time: " + new DecimalFormat("0.##").format(seconds) + "s.";
    }

    public long getN()
    {
        return n;
    }

    public BigInteger getCalculatedValue()
    {
        return calculatedValue;
    }

    public float getSeconds()
    {
        return seconds;
    }
}
