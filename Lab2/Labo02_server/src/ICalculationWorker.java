import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ICalculationWorker extends Remote
{
    BigIntegerResult calculate(CalculateParam param) throws RemoteException;
}
