public interface CalculateParam
{
    BigIntegerResult calculate();
}
