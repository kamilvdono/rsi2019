import java.util.Random;

public class Farmer
{
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";

    private ICalculationWorker[] workers;

    private FibonacciParam[] fibonacciParams = {
            new FibonacciParam(0),
            new FibonacciParam(500000),
            new FibonacciParam(280000),
            new FibonacciParam(-1),
            new FibonacciParam(5),
            new FibonacciParam(99999)
    };

    private FactorialParam[] factorialParams = {
            new FactorialParam(522334),
            new FactorialParam(22432),
            new FactorialParam(-1),
            new FactorialParam(1),
            new FactorialParam(243242),
            new FactorialParam(0)
    };

    public Farmer(ICalculationWorker[] workers)
    {
        this.workers = workers;
    }

    public void Run()
    {
        Thread [] threads = new Thread[fibonacciParams.length + factorialParams.length];
        int threadIndex = 0;
        for (int i = 0 ; i < fibonacciParams.length; i++, threadIndex++)
        {
            threads[threadIndex] = SendRequest(fibonacciParams[i]);
        }
        for (int i = 0; i < factorialParams.length; i++, threadIndex++)
        {
            threads[threadIndex] = SendRequest(factorialParams[i]);
        }

        awaitAll(threads);
    }

    private Thread SendRequest(CalculateParam param)
    {
         Thread thread = new Thread(() -> {
            try
            {
                Random random = new Random();
                BigIntegerResult result = workers[random.nextInt(workers.length)].calculate(param);
                System.out.println(result);
            }
            catch (Exception e)
            {
                System.out.println(ANSI_RED + "Exception while running request: " + e.getMessage() + ANSI_RESET);
            }
        });
        thread.start();
        return thread;
    }

    private void awaitAll(Thread [] threads)
    {
        try
        {
            for (Thread thread : threads)
            {
                thread.join();
            }
        }
        catch (InterruptedException e)
        {
            System.out.println("Something interrupted waiting!");
            e.printStackTrace();
        }
    }
}
