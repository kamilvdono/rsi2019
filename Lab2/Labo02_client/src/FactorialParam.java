import java.io.Serializable;
import java.math.BigInteger;

public class FactorialParam implements Serializable, CalculateParam
{
    private long n;

    public FactorialParam(long n)
    {
        this.n = n;
    }

    @Override
    public BigIntegerResult calculate()
    {
        if(n < 0)
        {
            throw new IllegalArgumentException("The n parameter is negative!");
        }
        long startTime = System.nanoTime();

        BigInteger fact = BigInteger.valueOf(1L);
        for (long i = 2; i <= n; i++)
        {
            fact = fact.multiply(BigInteger.valueOf(i));
        }

        float time = ((System.nanoTime() - startTime)/1000000000f);
        return new BigIntegerResult(n, fact, time, getClass().getName());
    }
}
