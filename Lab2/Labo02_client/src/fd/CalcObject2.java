package fd;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CalcObject2 extends Remote, Serializable
{
    ResultType calculate(InputType inputParam) throws RemoteException;
}
