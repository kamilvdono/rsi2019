import java.rmi.Naming;
import java.util.HashMap;
import java.util.Map;

public class Main
{
    private static final int cantRetrieveReferenceErrorCode = 999;

    public static void main(String [] args)
    {
        initSecurityManager();

        Map<String, ICalculationWorker> workers = setupWorkerProperties();

        new Farmer(workers.values().toArray(new ICalculationWorker[0])).Run();
    }

    private static void initSecurityManager()
    {
        if(System.getSecurityManager() == null)
        {
            System.setSecurityManager(new SecurityManager());
        }
    }

    private static Map<String, ICalculationWorker> setupWorkerProperties()
    {
        String [] workerAddresses =  { "//localhost/server1", "//localhost/server2" };

        Map<String, ICalculationWorker> workers = new HashMap<>(workerAddresses.length);
        try
        {
            for (String address : workerAddresses)
            {
                workers.put(address, (ICalculationWorker)Naming.lookup(address));
            }
            System.out.println("Reference to workers is retrieved");
        }
        catch (Exception e)
        {
            System.out.println("Invalid worker properties!");
            e.printStackTrace();
            System.exit(cantRetrieveReferenceErrorCode);
        }
        return workers;
    }
}
