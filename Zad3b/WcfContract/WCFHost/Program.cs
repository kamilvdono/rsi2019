﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using WcfContract;

namespace WCFHost
{
	class Program
	{
		static void Main(string[] args)
		{

			ServiceHost mojHost = new ServiceHost(typeof(Calculator));
			try
			{
				ServiceEndpoint endpoint = mojHost.Description.Endpoints.Find(new Uri("http://localhost:20000/calc/endpoint3"));
				Uri addressTCP = new Uri("net.tcp://localhost:20000/TCP");
				ServiceEndpoint endpoint2 = mojHost.AddServiceEndpoint(typeof(ICalculator), new NetTcpBinding(), addressTCP);
				//wyswietl endpointy

				Console.WriteLine("\nService endpoint {0}:", endpoint.Name);
				Console.WriteLine("Binding: {0}", endpoint.Binding.ToString());
				Console.WriteLine("ListenUri: {0}", endpoint.ListenUri.ToString());

				Console.WriteLine("\nService endpoint {0}:", endpoint2.Name);
				Console.WriteLine("Binding: {0}", endpoint2.Binding.ToString());
				Console.WriteLine("ListenUri: {0}", endpoint2.ListenUri.ToString());

				mojHost.Open();
				Console.WriteLine("\n--> Serwis 1 jest uruchomiony.");

				ContractDescription cd = ContractDescription.GetContract(typeof(ICalculator));
				Console.WriteLine("Informacje o kontrakcie:");
				Type contractType = cd.ContractType;
				Console.WriteLine("\tContract type: {0}", contractType.ToString());
				string name = cd.Name;
				Console.WriteLine("\tName: {0}", name);
				OperationDescriptionCollection odc = cd.Operations;
				Console.WriteLine("\tOperacje:");
				foreach (OperationDescription od in odc)
				{
					Console.WriteLine("\t\t" + od.Name);
				}

				Console.WriteLine("\n--> Nacisnij <ENTER> aby zakonczyc.");
				Console.WriteLine();
				Console.ReadLine();
				mojHost.Close();

			}
			catch (CommunicationException ce)
			{
				Console.WriteLine("Wystapil wyjatek: {0}", ce.Message);
				mojHost.Abort();
			}
		}
	}
}
