/**
 * Kamil Socha, 238054
 */

import org.apache.xmlrpc.WebServer;
import org.apache.xmlrpc.XmlRpcClient;

import java.util.Scanner;
import java.util.Vector;

//10.182.238.74

public class Main
{
	private static int PORT = 10000+13;

	public static void main(String[] args)
	{
		System.out.println("Podaj adres ip:");
		Scanner scanner = new Scanner(System.in);
		String ip = scanner.nextLine();
		if("".equals(ip)){
			ip = "127.0.1.0";
		}
		System.out.println("Podaj port:");
		String port = scanner.nextLine();
		if("".equals(port)){
			port = "10013";
		}
		try
		{
			XmlRpcClient srv = new XmlRpcClient("http://"+ip+":"+port);

			Show(srv);

			while(true){
				System.out.println("Wpisz nazwę metody aby wywolac ja, wpisz q aby wyjsc");
				String line = scanner.nextLine().trim();
				if(line.equals("q")){
					return;
				}
				else if(line.equals("Add")){
					Add(srv);
				}
				else if(line.equals("Mul")){
					Mul(srv);
				}
				else if(line.equals("Lerp")){
					Lerp(srv);
				}
				else if(line.equals("MulString")){
					MulString(srv);
				}
				else if(line.equals("CharCount")){
					CharCount(srv);
				}
				else if(line.equals("PrimeCount")){
					PrimeCount(srv);
				}
				else if(line.equals("Show")){
					Show(srv);
				}
				else{
					System.out.println("Nie znam komendy "+line+":(");
					System.out.println("Znane komendy:");
					Show(srv);
				}
			}
		}
		catch (Exception exception)
		{
			System.err.println("Klient XML-RPC: " +exception);
		}
	}

	public static void Show(XmlRpcClient srv) throws Exception
	{
		System.out.println((String)srv.execute("MathServer.Show", new Vector<Object>()));
	}

	public static void Add(XmlRpcClient srv) throws Exception
	{
		Scanner scanner = new Scanner(System.in);

		Vector<Integer> params = new Vector<Integer>();
		System.out.println("Podaj x");
		params.addElement(new Integer(scanner.nextInt()));
		System.out.println("Podaj y");
		params.addElement(new Integer(scanner.nextInt()));
		Object result = srv.execute("MathServer.Add", params);
		int wynik = ((Integer) result).intValue();
		System.out.println("Wynik: "+wynik);
	}

	public static void Mul(XmlRpcClient srv) throws Exception
	{
		Scanner scanner = new Scanner(System.in);

		Vector<Integer> params = new Vector<Integer>();
		System.out.println("Podaj x");
		params.addElement(new Integer(scanner.nextInt()));
		System.out.println("Podaj y");
		params.addElement(new Integer(scanner.nextInt()));
		Object result = srv.execute("MathServer.Mul", params);
		int wynik = ((Integer) result).intValue();
		System.out.println("Wynik: "+wynik);
	}

	public static void Lerp(XmlRpcClient srv) throws Exception
	{
		Scanner scanner = new Scanner(System.in);

		Vector<Double> params = new Vector<Double>();
		System.out.println("Podaj a");
		params.addElement(scanner.nextDouble());
		System.out.println("Podaj b");
		params.addElement(scanner.nextDouble());
		System.out.println("Podaj y");
		params.addElement(scanner.nextDouble());
		Object result = srv.execute("MathServer.Lerp", params);
		double wynik2 = ((Double) result).doubleValue();
		System.out.println("Wynik: "+wynik2);
	}

	public static void PrimeCount(XmlRpcClient srv) throws Exception
	{
		Scanner scanner = new Scanner(System.in);

		Vector<Integer> params = new Vector<Integer>();
		System.out.println("Podaj from");
		params.addElement(new Integer(scanner.nextInt()));
		System.out.println("Podaj n");
		params.addElement(new Integer(scanner.nextInt()));
		Object result = srv.execute("MathServer.PrimeCount", params);
		Vector<Integer> wynik = ((Vector<Integer>) result);
		System.out.println("Wynik: "+wynik);
	}

	public static void MulString(XmlRpcClient srv) throws Exception
	{
		Scanner scanner = new Scanner(System.in);

		Vector<Object> params = new Vector<Object>();
		System.out.println("Podaj ilosc powtorzen");
		params.addElement(new Integer(scanner.nextInt()));
		System.out.println("Podaj slowo");
		params.addElement(scanner.next().trim());
		System.out.println("Podaj czas uspienie odpowiedzi");
		params.addElement(new Integer(scanner.nextInt()));
		srv.executeAsync("MathServer.MulString", params, new AC());
	}

	public static void CharCount(XmlRpcClient srv) throws Exception
	{
		Scanner scanner = new Scanner(System.in);

		Vector<Object> params = new Vector<Object>();
		System.out.println("Podaj slowo");
		params.addElement(scanner.nextLine().trim());
		System.out.println("Podaj znak");
		params.addElement(scanner.nextLine().trim());
		System.out.println("Podaj czas uspienie odpowiedzi");
		params.addElement(new Integer(scanner.nextInt()));
		srv.executeAsync("MathServer.CharCount", params, new AC());
	}
}
